<head>
<script
  src="https://www.google.com/recaptcha/api.js?render=explicit"
  async defer>
</script>
<link rel="stylesheet" type="text/css" href="style.css">
</head>

<?php
include('environment.php');
session_unset();
?>

<div class='center'>
<noscript>
<div class='nojs error'>This tool requires javascript to be enabled.</div>
<div style="display:none"></noscript>

<span class='body'>

<form id='formbody' method=post action='address_lookup.ajax.php'>

Please enter your information:

<label class='input'>
<div class='question'>Name:</div>
<div class='response'><input type=text name=name></input></div>
</label>

<label class='input'>
<div class='question'>Email:</div>
<div class='response'><input type=text name=email></input></div>
</label>

<label class='input'>
<div class='question'>Street Address:</div>
<div class='response'><input type=text name=street></input></div>
</label>

<label class='input'>
<div class='question'>City:</div>
<div class='response'><input type=text name=city></input></div>
</label>

<label class='input'>
<div class='question'>Zip Code:</div>
<div class='response'><input type=text name=zip></input></div>
</label>

<br>

<label class='input'>
<div class='question'>
What issue concerns you?
</div>
<div class='response'>
<select name='issue'>
  <?php include "get_select_options.php"; ?>
</select>
</div>
</label>

<br><div class='error-field'>&nbsp;</div><br>

<input type=submit name='Submit' value='Submit' onclick='return submitInitialForm();' />

</form>

</span>
<noscript></div></noscript>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
</script>

<script>
function submitInitialForm() {
  $('div.error-field').html('<font color=green>One moment as we check that...</font>');
  $.post('address_lookup.ajax.php', $('#formbody').serialize(), processAddress);

  return false;
}

function processAddress(input) {
  var data;
  try {
    data = JSON.parse(input);
  } catch(e) {
    $('div.error-field').html('<font color=red>Something went wrong with your request.</font>');
    return;
  }

  if(data['name'] === false) {
    $('div.error-field').html('<font color=red>Please be sure to enter a valid name.</font>');
  } else if(data['email'] === false) {
    $('div.error-field').html('<font color=red>Please be sure to enter a valid email address.</font>');
  } else if(data['addr'] === false) {
    $('div.error-field').html('<font color=red>Unfortunately, we could not match that address.</font>');
  } else if(data['lookup_valid'] === false) {
    $('div.error-field').html('<font color=red>Unfortunately, we could not process that address.</font>');
  } else if(data['template_valid'] === false) {
    $('div.error-field').html('<font color=red>Unfortunately, the email template associated with that issue seems to be corrupted.</font>');
  } else {
    $('div.error-field').html('<font color=green>Please wait as we compose your email.</font>');
    reviewEmail(data);
  }
}

function reviewEmail(data) {
  var html;
  html  = "<div class='message'>Please review these emails:</div><br>";
  html += "<div class='email senator-email'>To: "+data['sen']+"<br>";
  html += "Subject: "+data['senSubject']+"<br>";
  html += "Body: "+data['senBody']+"</div>";
  html += "<div class='email rep-email'>To: "+data['rep']+"<br>";
  html += "Subject: "+data['repSubject']+"<br>";
  html += "Body: "+data['repBody']+"</div>";
  html += "</div>";
  html += "<div class='error-field'>&nbsp;</div>";
  html += "<div id='recaptcha-widget' class='g-recaptcha' data-sitekey='6LcyDwsUAAAAAMkBMn3a3TMHMZTPvU_sT1SaAG1m'></div>";
  html += "<input type=button value='Approve and Send' onclick='sendEmails();' />";
  $('#formbody').html(html);
  grecaptcha.render('recaptcha-widget', {"sitekey":"6LcyDwsUAAAAAMkBMn3a3TMHMZTPvU_sT1SaAG1m"});

  //document.title = "Review Email";
  //window.history.pushState({"html":"/review","pageTitle":"Review Email"}, "", "review");
}

function sendEmails() {
  $('div.error-field').html('<font color=green>Sending emails...</font>');
  $.post('send_emails.ajax.php', $('#formbody').serialize(), confirmSend);

  return false;
}

function confirmSend(input) {
  var html;
  if(input === '') {
    html  = '<font color=green>Thank you for participating in your government!</font>';
    html += '<script>setTimeout(location.reload.bind(location), 3000);<\/script>';
  } else {
    html = '<font color=red>Something went wrong with your request. Please ensure you did the captcha correctly.</font>';
  }
  $('div.error-field').html(html);
}
</script>

