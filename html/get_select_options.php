<?php
require_once('environment.php');

$dir = dir($DROPBOX);
while($file = $dir->read()) {
  if(preg_match('/^.*\.txt$/', $file)) {
    $text = file_get_contents("$dir->path/$file");

    $issue = array();
    preg_match('/^\s*Issue:\s*(.*)\s*$/m', $text, $issue);

    if(count($issue) >= 2) {
      $issue = $issue[1];
      echo "<option value='$file'>$issue</option>";
    }
  }
}

?>
