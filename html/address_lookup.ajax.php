<?php
require_once('environment.php');

//NOTE: THIS IS A TERRIBLE WAY TO DO THIS,
// BUT IT IS THE BEST WAY AVAILABLE
//
// Other options involve maintaining shapefiles
//  and GIS data for the state, which is not
//  feasible for the SGC team who will use this.

$results = array();
$results['lookup_valid'] = true;
$results['template_valid'] = true;
$results['issue'] = $_POST['issue'];

//checking the name is suuuuper easy and should be done first
if(!empty($_POST['name']))
  $results['name'] = $_POST['name'];
else {
  $results['name'] = false;
  echo json_encode($results);
  return;
}

//checking the email is pretty easy and should be done next
if(Email::ValidateEmail($_POST['email']))
  $results['email'] = $_POST['email'];
else {
  $results['email'] = false;
  echo json_encode($results);
  return;
}

//query the information
$Address1 = StringCleaner::CleanInput($_POST['street']);
$City = StringCleaner::CleanInput($_POST['city']);
$ZipCode = StringCleaner::CleanInput($_POST['zip']);

$search = "http://www.fyi.legis.state.tx.us/Address.aspx?Address1=$Address1&City=$City&ZipCode=$ZipCode&DistrictType=ALL&Submit1=Submit";

$whoRepsMe = file_get_contents($search);

//check for the Who Represents Me? error message
if(preg_match('/unable to match this street address/', $whoRepsMe) === 1) {
  $results['addr'] = false;
  echo json_encode($results);
  return;
}

//try to get some data out of the response
$senMatches = array();
preg_match_all('/<a [^>]*>Senator ([^<]*)</', $whoRepsMe, $senMatches);

$repMatches = array();
preg_match_all('/<a [^>]*>Representative ([^<]*)</', $whoRepsMe, $repMatches);

$addrMatches = array();
preg_match_all('/id="IncumbentDisplayBlock1_Table1"((?!<td>).)*<td>(((?!<\/td>).)*)<\/td>/s',
  $whoRepsMe, $addrMatches);

//check the data
if(count($addrMatches[2]) == 1) {
  $results['addr'] = str_replace("\n", '', $addrMatches[2][0]);
  $results['addr'] = str_replace('&nbsp;', '', $results['addr']);
  $results['addr'] = preg_replace('/<[\s\\/]*br[\s\\/]*>/i', "\n", $results['addr']);
} else
  $results['lookup_valid'] = false;

if(count($senMatches[1]) == 3)
  $results['sen'] = $senMatches[1][2];
else
  $results['lookup_valid'] = false;

if(count($repMatches[1]) == 1)
  $results['rep'] = $repMatches[1][0];
else
  $results['lookup_valid'] = false;

if($results['lookup_valid'] === false) {
  echo json_encode($results);
  return;
}

//compose the email
$issueFile = str_replace('/', '', $_POST['issue']);
$text = file_get_contents("$DROPBOX/$issueFile");

$subject = array();
preg_match('/^\s*Subject:\s*(.*)\s*$/m', $text, $subject);

$body = array();
preg_match('/^\s*Body:\s*(.*)$/ms', $text, $body);

if(count($subject) == 2) {
  $results['repSubject'] = StringCleaner::CleanOutput_Rep($subject[1], $results);
  $results['senSubject'] = StringCleaner::CleanOutput_Sen($subject[1], $results);
} else
  $results['template_valid'] = false;

if(count($body) == 2) {
  $results['repBody'] = StringCleaner::CleanOutput_Rep($body[1], $results);
  $results['senBody'] = StringCleaner::CleanOutput_Sen($body[1], $results);
} else
  $results['template_valid'] = false;

if($results['template_valid'] === false) {
  echo json_encode($results);
  return;
}

//store the emails in a session variable to avoid hacking
foreach($results as $key=>&$result) {
  $_SESSION[$key] = $result;
  $result = nl2br(htmlspecialchars($result));
}

echo json_encode($results);

?>
