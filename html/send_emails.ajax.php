<?php
require_once('environment.php');

//validate the captcha
$recapURL = "https://www.google.com/recaptcha/api/siteverify";
$recapData = array(
  "secret"   => $RECAP_KEY,
  "response" => $_POST['g-recaptcha-response'],
);
$recapOptions = array(
  "http" => array(
    "header"  => "Content-type: application/x-www-form-urlencoded\r\n",
    "method"  => "POST",
    "content" => http_build_query($recapData),
  )
);
$recapContext = stream_context_create($recapOptions);
$result = file_get_contents($recapURL, false, $recapContext);
$result = json_decode($result, true);

if(!$result['success']) {
  echo "Captcha failed!";
  return false;
}

//validate the session variables
if(empty($_SESSION['rep']) ||
  empty($_SESSION['sen']) ||
  empty($_SESSION['name']) ||
  empty($_SESSION['email']) ||
  empty($_SESSION['senSubject']) ||
  empty($_SESSION['senBody']) ||
  empty($_SESSION['repSubject']) ||
  empty($_SESSION['repBody']) ) {
    echo "Session not set!";
    return false;
  }

//find a rep email from their name
function GetEmailFromName($name) {
  $name = trim($name);

  $first = array();
  preg_match('/^(.*)\s/', $name, $first);
  $first = $first[1];

  $last = array();
  preg_match('/\s(.*)$/', $name, $last);
  $last = $last[1];

  return strtolower("$first.$last");
}

$repEmail = $_SESSION['rep']." <".GetEmailFromName($_SESSION['rep'])."@house.state.tx.us>";
$senEmail = $_SESSION['sen']." <".GetEmailFromName($_SESSION['sen'])."@senate.state.tx.us>";

//build the email to the students
$studentEmail = "{$_SESSION['name']} <{$_SESSION['email']}>";
$studentSubject = $_SESSION['senSubject'];
$studentBody = "Dear {$_SESSION['name']},
";

if(!$settings->sending()) {
  $studentBody .= "
-----------------------------------------

NOTE: At this time, this is the only email sent.
This system is currently not emailing legislators.

-----------------------------------------
";
}

$studentBody .= "
On your behalf, we sent the following two emails:

To: $senEmail
Subject: {$_SESSION['senSubject']}
Body: {$_SESSION['senBody']}

To: $repEmail
Subject: {$_SESSION['repSubject']}
Body: {$_SESSION['repBody']}";

//send the emails
Email::SendEmail($_SESSION['email'], $_SESSION['email'], $studentSubject, $studentBody);

if($settings->sending()) {
  Email::SendEmail($senEmail, $_SESSION['email'], $_SESSION['senSubject'], $_SESSION['senBody']);
  Email::SendEmail($repEmail, $_SESSION['email'], $_SESSION['repSubject'], $_SESSION['repBody']);
}

?>
