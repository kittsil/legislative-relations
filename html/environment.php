<?php

$RECAP_KEY = 'REMOVED FOR SECURITY -- register the domain with google to get a new one';

$BASEDIR = __dir__;
$UTILDIR = preg_replace('/html$/', 'utils', $BASEDIR);

$DROPBOX = '/home/ec2-user/Dropbox/MailerConfiguration';

require_once "$UTILDIR/autoload.php";

global $settings;
if(empty($settings))
  $settings = new Settings();

if(!$settings->online()) {
  echo "<div class='center'>";
  echo "Unfortunately, the Legislative contact website is currently disabled.";
  echo "</div>";
  exit;
}

session_start();

?>
