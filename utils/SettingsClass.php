<?php

if(empty($INC_SETTINGSCLASS))
  $INC_SETTINGSCLASS = 1;
else
	return;

class Settings {

  private $online;
  private $sending;

  function Settings() {
    $this->online = false;
    $this->sending = false;

    global $DROPBOX;
    $settingsFile = "$DROPBOX/settings.txt";
    if(!file_exists($settingsFile))
      return;

    $settings = file_get_contents("$DROPBOX/settings.txt");
    
    if($settings === false)
      return;

    $online = array();
    preg_match('/^\s*Online:\s*(Yes|No)\s*$/im', $settings, $online);

    if(count($online) >= 2)
      $this->online = (strtolower($online[1]) === 'yes');
      
    $sending = array();
    preg_match('/^\s*Sending:\s*(Yes|No)\s*$/im', $settings, $sending);
    
    if(count($sending) >= 2)
      $this->sending = (strtolower($sending[1]) === 'yes');
  }

  function Online() {
    return $this->online;
  }

  function Sending() {
    return $this->sending;
  }

}; //class Settings

?>
