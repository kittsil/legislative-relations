<?php

if(empty($INC_STRINGCLEANERCLASS))
  $INC_STRINGCLEANERCLASS = 1;
else
	return;

class StringCleaner {

  //get the input in the format used by fyi.legis.state.tx.us
  static function CleanInput($input) {
    return preg_replace('/\s+/', '+', $input);
  }

  //clean up email to send
  private static function CleanOutput($string, $params) {
    $string = str_replace('#Name#', $params['name'], $string);
    $string = str_replace('#Address#', $params['addr'], $string);
    $string = str_replace('#Email#', $params['email'], $string);
    return $string;
  }

  static function CleanOutput_Sen($string, $params) {
    $string = str_replace('#Representative#', 'Senator '.$params['sen'],
      $string);
    return self::CleanOutput($string, $params);
  }

  static function CleanOutput_Rep($string, $params) {
    $string = str_replace('#Representative#', 'Representative '.$params['rep'],
      $string);
    return self::CleanOutput($string, $params);
  }
}; //class StringCleaner

?>
