<?php

spl_autoload_register(
  function($ClassName) {
    global $UTILDIR;
    include "$UTILDIR/{$ClassName}Class.php";
  },
  true
);

?>
