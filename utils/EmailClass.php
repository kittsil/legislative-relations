<?php

if(empty($INC_EMAILCLASS))
  $INC_EMAILCLASS = 1;
else
	return;

class Email {
class Email {
  static function SendEmail($to, $from, $subject, $body) {
    global $DROPBOX;
    //for now, all email comes from this address
    $lr = 'Texas Student Government Coalition <texassgc@mailer.texassgc.com>';

    //logging information
    date_default_timezone_set('America/Chicago');
    $ret = date('d M Y - H:i');
    $ret .= "\n\tFrom: $from\n\tTo: $to\n\tSubject: $subject\n";

    $hash = md5(time());

    $headers  = "From: $lr".PHP_EOL;
    //$headers  = "Sender: $lr".PHP_EOL;
    $headers .= "Reply-to: $from".PHP_EOL;
    $headers .= "MIME-Version: 1.0".PHP_EOL;
    $headers .= "Content-Type: multipart/mixed; boundary=\"$hash\"".PHP_EOL;
    $headers .= "Content-Transfer-Encoding: 7bit".PHP_EOL;
    $headers .= "This is a MIME encoded message.".PHP_EOL.PHP_EOL;

    // message
    $message = "--$hash".PHP_EOL;
    $message .= "Content-Type: text/plain; charset=\"iso-8859-1\"".PHP_EOL;
    $message .= "Content-Transfer-Encoding: base64".PHP_EOL.PHP_EOL;
    $message .= chunk_split(base64_encode($body), 76, PHP_EOL).PHP_EOL.PHP_EOL;

    $message .= "--$hash".PHP_EOL;

    if (!mail($to, $subject, $message, $headers)) {
      file_put_contents("$DROPBOX/log.txt", $ret."\tNOT SENT\n", FILE_APPEND);
      echo "There was a problem sending this email.";
      return false;
    } else {
      file_put_contents("$DROPBOX/log.txt", $ret, FILE_APPEND);
      return true;
    }
  }

  static function ValidateEmail(&$email) {
    //trim the email
    $email = trim($email);

    if(empty($email))
      return false;

    //check syntax
    $az09 = "[a-zA-Z0-9_-]+";
    $pattern = "/^$az09(\.$az09)*@$az09(\.$az09)+$/";
    if(!preg_match($pattern, $email))
      return false;

    //check domain name in DNS records
    list($username,$domain)=split('@',$email);
    if(!checkdnsrr($domain,'MX'))
      return false;

    return true;
  }

}; //class Email

?>
